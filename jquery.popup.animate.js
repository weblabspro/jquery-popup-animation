(function($) {

	$.fn.popupToggle = function(settings) {
		var opts = $.extend({}, $.fn.popupToggle.defaults, settings);

		var bodyOverflow = $('body').css('overflow');

		var $this = $(this);
		var $modal = $this.find('.modal'),
			$close = $this.find('.close'),
			$confirm = $this.find('.confirm, *[type=submit]'),
			$okey = $this.find('.okey');

		var show = function() {
			var basicMagrinTop = $modal.css('margin-top');
			$modal.css({
				'margin-top': $modal.height()
			});
			$this.css({
				opacity: 0
			});
			$this.show();

			$this.animate({
				opacity: 1
			}, {
				easing: 'easeOutCubic',
				duration: opts.duration
			});

			$modal.animate({
				'margin-top': basicMagrinTop
			}, {
				easing: 'easeOutCubic',
				duration: opts.duration
			});

			$('body').css('overflow', 'hidden');
		};

		var close = function() {
			$this.animate({
				opacity: 0
			}, {
				easing: 'easeOutCubic',
				duration: opts.duration,
				complete: function() {
					$this.hide();
				}
			});

			$modal.animate({
				'margin-top': $modal.height()
			}, {
				easing: 'easeOutCubic',
				duration: opts.duration
			});

			$('body').css('overflow', bodyOverflow);
		};

		show();

		$(document).keyup(function(e) {
			if(27 == e.keyCode) {
				close();
			}
		});

		$modal.click(function(e) {
			e.stopPropagation();
		});
		$this.click(function() {
			close();
		});
		$close.click(function() {
			close();
		});
		$confirm.click(function() {
			close();

			if('undefined' !== typeof opts.success && !$(this).hasClass('hide-emassage')) {
				setTimeout(function() {
					Emessage(opts.success);
				}, opts.duration*2);
			}
		});
		$okey.click(function() {
			close();
		});
	};

	$.fn.popupToggle.defaults = {
		duration: 200
	};

})(jQuery);